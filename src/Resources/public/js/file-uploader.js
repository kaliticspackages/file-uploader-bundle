document.addEventListener(
    'DOMContentLoaded',
    () => {
        const init = (
            embryo
        ) => {
            const dataset = embryo.dataset;

            if (embryo.id === '') {
                throw new Error(
                    'file_uploader: missing id',
                )
            }

            const input = embryo.querySelector(`input[type="file"]`);
            if (!(input instanceof HTMLInputElement)) {
                throw new Error(
                    'file_uploader: missing input',
                );
            }

            const coreConfig = {
                autoProceed: true,
                debug: true,
                restrictions: {
                    allowedFileTypes: dataset.accept === undefined
                        ? null
                        : dataset.accept.split(','),
                    maxFileSize: dataset.maxsize === undefined
                        ? null
                        : parseInt(dataset.maxsize),
                    maxNumberOfFiles: dataset.multiple === 'multiple'
                        ? null
                        : 1,
                    minNumberOfFiles: dataset.required === 'required'
                        ? 1
                        : null,
                },
            };
            const dashboardConfig = {
                disabled: dataset.disabled === 'disabled',
                inline: true,
                plugins: [
                    'Webcam',
                ],
                height: dataset.height === undefined
                    ? 550
                    : parseInt(dataset.height),
                hideProgressAfterFinish: true,
                showProgressDetails: true,
                showRemoveButtonAfterComplete: true,
                target: embryo,
                theme: 'auto',
                waitForThumbnailsBeforeUpload: true,
                width: dataset.width === undefined
                    ? 750
                    : parseInt(dataset.width),
            };
            const xhruploadConfig = {
                endpoint: dataset.action,
                limit: 1,
            };

            console.group(`File Uploader : ${embryo.id}`);
            console.log('Core', coreConfig);
            console.log('Dashboard', dashboardConfig);
            console.log('XHRUpload', xhruploadConfig);
            console.groupEnd();

            const uppy = Uppy.Core(coreConfig);

            uppy.use(
                Uppy.Dashboard,
                dashboardConfig,
            );
            uppy.use(
                Uppy.XHRUpload,
                xhruploadConfig,
            );

            const triggerForm = () => {
                const container = new DataTransfer();

                uppy
                    .getFiles()
                    .filter(
                        (
                            file,
                        ) => {
                            return file.response !== undefined;
                        }
                    )
                    .map(
                        (
                            file,
                        ) => {
                            return new File(
                                [
                                    file.name,
                                ],
                                file.response.body.file,
                                {
                                    type: file.response.body.mimetype,
                                }
                            );
                        }
                    )
                    .forEach(
                        (
                            file,
                        ) => {
                            container.items.add(file);
                        }
                    );

                input.files = container.files;

                $(input).change();
            };

            uppy.on(
                'complete',
                triggerForm,
            );
            uppy.on(
                'file-removed',
                triggerForm,
            );
        };

        let timeoutId = null;
        const triggerInit = () => {
            clearTimeout(timeoutId);
            timeoutId = window.setTimeout(
                () => {
                    const embryos = document.querySelectorAll('div[data-type="file-uploader"][data-embryo]');

                    for (const embryo of embryos) {
                        if (embryo instanceof HTMLElement) {
                            delete embryo.dataset.embryo;
                        }
                    }
                    for (const embryo of embryos) {
                        if (embryo instanceof HTMLElement) {
                            init(embryo);
                        }
                    }
                },
                300,
            );
        };

        const mutationObserver = new MutationObserver(
            (
                records
            ) => {
                records.forEach(
                    (
                        record
                    ) => {
                        if (record.addedNodes.length > 0) {
                            triggerInit();
                        }
                    }
                );
            }
        );
        mutationObserver.observe(
            document,
            {
                childList: true,
                subtree: true,
            }
        );
    }
);
