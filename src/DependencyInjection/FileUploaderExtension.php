<?php

declare(strict_types = 1);

namespace Kalitics\FileUploaderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

use function vsprintf;

class FileUploaderExtension
    extends Extension
    implements PrependExtensionInterface{

    public const PREPEND_BUNDLE = 'oneup_uploader';

    /**
     * @param array                                                   $configs
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     *
     * @return void
     * @throws \Exception
     */
    public function load(
        array $configs,
        ContainerBuilder $container
    ) : void {
        $fileLocator = new FileLocator(
            __DIR__ . '/../Resources/config',
        );
        $loader      = new YamlFileLoader(
            $container,
            $fileLocator,
        );
        $loader->load('services.yaml');
    }

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     *
     * @return void
     * @throws \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     * @throws \Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException
     */
    public function prepend(
        ContainerBuilder $container
    ) : void {
        if(!$container->hasExtension(self::PREPEND_BUNDLE)) {
            $message = vsprintf(
                "Missing extension: '%s'",
                [
                    self::PREPEND_BUNDLE,
                ],
            );

            throw new InvalidConfigurationException(
                $message,
            );
        }

        $parameterBag             = $container->getParameterBag();
        $parameterKernelCacheDir  = $parameterBag->get('kernel.cache_dir');
        $parameterUploadDirectory = $parameterBag->get('upload_directory');

        $cacheDirectory = "{$parameterKernelCacheDir}/uploads";

        $container->prependExtensionConfig(
            self::PREPEND_BUNDLE,
            [
                'chunks'    => [
                    'maxage'  => 3600,
                    'storage' => [
                        'directory' => "{$cacheDirectory}/chunks",
                    ],
                ],
                'mappings'  => [
                    'gallery' => [
                        'enable_cancelation' => true,
                        'enable_progress'    => true,
                        // HACK
                        'frontend'           => 'dropzone',
                        'storage'            => [
                            'directory' => "{$parameterUploadDirectory}/files",
                        ],
                        'use_orphanage'      => true,
                    ],
                ],
                'orphanage' => [
                    'maxage'    => 86400,
                    'directory' => "{$cacheDirectory}/orphans",
                ],
            ],
        );
    }
}
