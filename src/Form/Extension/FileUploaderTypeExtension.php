<?php

declare(strict_types = 1);

namespace Kalitics\FileUploaderBundle\Form\Extension;

use Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;

use function file_get_contents;
use function is_array;
use function iterator_to_array;

class FileUploaderTypeExtension
    extends AbstractTypeExtension{

    /** @var \Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage */
    private FilesystemOrphanageStorage $orphanageStorage;

    /**
     * @param \Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage $orphanageStorage
     */
    public function __construct(
        FilesystemOrphanageStorage $orphanageStorage
    ) {
        $this->orphanageStorage = $orphanageStorage;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     *
     * @return void
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ) : void {
        parent::buildForm(
            $builder,
            $options
        );

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function(
                PreSubmitEvent $preSubmitEvent
            ) : PreSubmitEvent {
                $multiple = true;
                $realFiles = [];

                $orphanFiles = $this->orphanageStorage
                    ->getFiles()
                    ->files();

                $fakeFiles = $preSubmitEvent->getData();
                if(!is_array($fakeFiles)) {
                    $fakeFiles = [$fakeFiles];
                    $multiple  = false;
                }

                foreach($fakeFiles as $fakeFile) {
                    $files = clone $orphanFiles;
                    if($fakeFile instanceof UploadedFile) {
                        $name     = $fakeFile->getClientOriginalName();
                        $mimetype = $fakeFile->getClientMimeType();

                        $files     = $files->name($name);
                        $fileInfos = iterator_to_array($files);
                        $path      = key($fileInfos);

                        if($path === null) {
                            $path         = $fakeFile->getRealPath();
                            $originalName = $name;
                        } else {
                            $originalName = file_get_contents($fakeFile->getRealPath());
                        }

                        $realFiles[] = new UploadedFile(
                            $path,
                            $originalName,
                            $mimetype,
                            null,
                            true,
                        );
                    }
                }

                if(!$multiple) {
                    $realFiles = reset($realFiles)
                        ?: null;
                }

                $preSubmitEvent->setData($realFiles);

                return $preSubmitEvent;
            }
        );
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     *
     * @return void
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(
        OptionsResolver $resolver
    ) : void {
        parent::configureOptions($resolver);

        $resolver->setDefault(
            'mapped',
            false
        );
    }

    /**
     * Return the class of the type being extended.
     */
    public static function getExtendedTypes() : iterable {
        return [
            FileType::class,
        ];
    }
}
