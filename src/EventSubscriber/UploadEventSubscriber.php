<?php

declare(strict_types = 1);

namespace Kalitics\FileUploaderBundle\EventSubscriber;

use Oneup\UploaderBundle\Event\PostUploadEvent;
use Oneup\UploaderBundle\UploadEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UploadEventSubscriber implements EventSubscriberInterface{

    /**
     * @return string[]
     */
    public static function getSubscribedEvents() : array {
        return [
            UploadEvents::POST_UPLOAD => 'onPostUpload',
        ];
    }

    /**
     * @param \Oneup\UploaderBundle\Event\PostUploadEvent $postUploadEvent
     *
     * @return \Oneup\UploaderBundle\Event\PostUploadEvent
     */
    public function onPostUpload(
        PostUploadEvent $postUploadEvent
    ) : PostUploadEvent {
        $file     = $postUploadEvent->getFile();
        $response = $postUploadEvent->getResponse();

        // NOTE: response implements ArrayAccess
        $response['file']     = $file->getFilename();
        $response['mimetype'] = $file->getMimeType();

        return $postUploadEvent;
    }
}
