# FileUploaderBundle

Standardize file uploader features

## How to use
1. require package `composer require kalitics/file-uploader-bundle`
2. install the assets `php bin/console assets:install`
3. link assets in your templates
```html
<link href="{{ asset('bundles/kaliticsfileuploader/css/uppy.min.css') }}" rel="stylesheet">

<script src="{{ asset('bundles/kaliticsfileuploader/js/uppy.min.js') }}"></script>
<script src="{{ asset('bundles/kaliticsfileuploader/js/file-uploader.js') }}"></script>
```
4. Add routing
```yaml
# config/routes.yaml

file_uploader_bundle:
  resource: '@FileUploaderBundle/Resources/config/routes.yaml'
```
5. Add form theme in twig config (if you want to set globally)
```yaml
# config/packages/twig.yaml
form_themes:
  ...
  - '@FileUploader/form/_theme.html.twig'
```
6. Render a file input via twig
```twig
{# if file input is form.file #}
{{ form_row(form.file) }}
{{ form_widget(form.file) }}
```
7. Profit

## How to disable
Change form theme for form OR input
```twig
{# if file input is form.file #}
{% form_theme form[.file] 'THEME.html.twig' %}

...
```

## Caveat
IF a form theme is set for a particular form, DO NOT forget to reset the file input's form theme
```twig
{# if file input is form.file #}
{% form_theme form 'THEME.html.twig' %}
{% form_theme form.file '@FileUploader/form/_theme.html.twig' %}

...
```
